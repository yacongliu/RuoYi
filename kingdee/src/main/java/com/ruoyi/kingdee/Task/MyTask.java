package com.ruoyi.kingdee.Task;

import org.springframework.stereotype.Component;

/**
 * 后台任务测试类（bean方式）
 *
 * @author yacong_liu
 * @version 1.0
 * @classname MyTask
 * @date 2021/9/20
 **/
@Component("myTask")
public class MyTask {

    public void process(String msg) {
        System.out.println(String.format("****MyTask invoke msg: %s", msg));
    }
}
