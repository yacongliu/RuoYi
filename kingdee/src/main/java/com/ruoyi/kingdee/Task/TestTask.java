package com.ruoyi.kingdee.Task;


/**
 * 后台定时任务测试类(全限定名)
 *
 * @author yacong_liu
 * @version 1.0
 * @classname TestTask
 * @date 2021/9/19
 **/
public class TestTask {
    public void process(String msg) {
        System.out.println(String.format("****TestTask invoke msg is : %s", msg));
    }

    public static void main(String[] args) {
        new TestTask().process("hello Task!");

    }
}
